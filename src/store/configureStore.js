/**
 * Created by badra on 8/2/2016.
 */

import {createStore, applyMiddleware} from 'redux';
import reduxImmutableStateVariant from 'redux-immutable-state-invariant';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';


export default function (initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunk, reduxImmutableStateVariant()));
}




