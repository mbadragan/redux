/**
 * Created by badra on 8/2/2016.
 */
import ActionTypes from './actionTypes';
import CourseApi from '../api/mockCourseApi';
import {browserHistory} from 'react-router';
import initialState from '../reducers/initialState';

export function getAllCourses() {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.GET_ALL_COURSES_PENDING
    });

    CourseApi.getAllCourses().then(function (courses) {
      dispatch({
        type: ActionTypes.GET_ALL_COURSES_SUCCESS,
        courses: courses
      });

    });
  };
}

export function saveCourse(course) {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.SAVE_COURSE_PENDING
    });

    CourseApi.saveCourse(course).then(function () {
      dispatch({
        type: ActionTypes.SAVE_COURSE_SUCCESS,
      });

      browserHistory.push('/courses');
    });
  };
}

export function setCurrentCourse(course) {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.SET_CURRENT_COURSE,
      course: course
    });
  }
}

export function resetCurrentCourse() {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.RESET_CURRENT_COURSE
    });
  };
}

export function getCourseById(id) {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.GET_COURSE_PENDING
    });

    CourseApi.getCourseById(id).then( function (course) {
      dispatch({
        type: ActionTypes.GET_COURSE_SUCCESS,
        course: course
      });
    });
  }
}


