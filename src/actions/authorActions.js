/**
 * Created by badra on 8/4/2016.
 */
/**
 * Created by badra on 8/2/2016.
 */
import ActionTypes from './authorActionTypes';
import AuthorApi from '../api/mockAuthorApi';
import {browserHistory} from 'react-router';

export function getAllAuthors() {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.GET_ALL_AUTHORS_PENDING
    });

    AuthorApi.getAllAuthors().then(function (authors) {
      // console.log('get all authors', authors);
      dispatch({
        type: ActionTypes.GET_ALL_AUTHORS_SUCCESS,
        authors: authors
      });

    });
  };
}

export function saveAuthor(author) {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.SAVE_AUTHOR_PENDING
    });

    AuthorApi.saveAuthor(author).then(function () {
      dispatch({
        type: ActionTypes.SAVE_AUTHOR_SUCCESS,
      });

      browserHistory.push('/authors');
    });
  };
}

export function setCurrentAuthor(author) {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.SET_CURRENT_AUTHOR,
      author: author
    });
  }
}

export function resetCurrentAuthor() {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.RESET_CURRENT_AUTHOR
    });
  };
}

export function getAuthorById(id) {
  return (dispatch) => {
    dispatch({
      type: ActionTypes.GET_AUTHOR_PENDING
    });

    AuthorApi.getAuthorById(id).then( function (author) {
      dispatch({
        type: ActionTypes.GET_AUTHOR_SUCCESS,
        author: author
      });
    });
  }
}


