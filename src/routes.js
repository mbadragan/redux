/**
 * Created by badra on 8/1/2016.
 */

import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import CoursePage from './components/course/CoursePage';
import AuthorPage from './components/author/AuthorPage';
import ManageCoursePage from './components/course/ManageCoursePage';
import ManageAuthorPage from './components/author/ManageAuthorPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="about" component={AboutPage}/>
    <Route path="courses/add" component={ManageCoursePage}/>
    <Route path="courses/:id" component={ManageCoursePage}/>
    <Route path="authors/add" component={ManageAuthorPage}/>
    <Route path="authors/:id" component={ManageAuthorPage}/>
    <Route path="courses" component={CoursePage}/>
    <Route path="authors" component={AuthorPage}/>
  </Route>
);
