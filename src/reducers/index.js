/**
 * Created by badra on 8/2/2016.
 */

import {combineReducers} from 'redux';
import courses from './courseReducer';
import authors from './authorReducer';

export default combineReducers({
  courses,
  authors
});
