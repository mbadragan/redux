/**
 * Created by badra on 8/2/2016.
 */
import ActionTypes from '../actions/actionTypes';
import initialState from './initialState';
import {browserHistory} from 'react-router';

export default (state = initialState.courses, action) => {
  switch (action.type) {
    case ActionTypes.GET_COURSE_PENDING:
      return {
        ...state,
        pending: true
      };

    case ActionTypes.GET_COURSE_SUCCESS:
      return {
        ...state,
        pending: false,
        current: action.course
      };

    case ActionTypes.GET_ALL_COURSES_SUCCESS:
      return {
        ...state,
        pending: false,
        list: action.courses
      };

    case ActionTypes.GET_ALL_COURSES_PENDING:
      return {
        ...state,
        pending: true
      };

    case ActionTypes.CREATE_COURSE_SUCCESS:
      return {
        ...state,
        courses: [
          ...state.courses,
          Object.assign({}, action.course)
        ]
      };
    // return [
    //   ...state,
    //   Object.assign({}, action.course)
    // ];

    case ActionTypes.SAVE_COURSE_PENDING:
      return {
        ...state,
        pending: true
      };

    case ActionTypes.SAVE_COURSE_SUCCESS:
      return {
        ...state,
        pending: false
      };
    case ActionTypes.SET_CURRENT_COURSE:
      return {
        ...state,
        current: Object.assign({}, action.course)
      };

    case ActionTypes.RESET_CURRENT_COURSE:
      return {
        ...state,
        current: initialState.courses.current
      };
    default:
      return state;
  }
};

