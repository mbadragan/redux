/**
 * Created by badra on 8/4/2016.
 */
/**
 * Created by badra on 8/2/2016.
 */
import ActionTypes from '../actions/authorActionTypes';
import initialState from './initialState';
import {browserHistory} from 'react-router';

export default (state = initialState.authors, action) => {
  switch (action.type) {
    case ActionTypes.GET_AUTHOR_PENDING:
      return {
        ...state,
        pending: true
      };

    case ActionTypes.GET_AUTHOR_SUCCESS:
      return {
        ...state,
        pending: false,
        current: action.author
      };

    case ActionTypes.GET_ALL_AUTHORS_SUCCESS:
      return {
        ...state,
        pending: false,
        list: action.authors
      };

    case ActionTypes.GET_ALL_AUTHORS_PENDING:
      return {
        ...state,
        pending: true
      };

    case ActionTypes.CREATE_AUTHOR_SUCCESS:
      return {
        ...state,
        pending: false,
        authors: [
          ...state.authors,
          Object.assign({}, action.author)
        ]
      };

    case ActionTypes.SAVE_AUTHOR_PENDING:
      return {
        ...state,
        pending: true
      };

    case ActionTypes.SAVE_AUTHOR_SUCCESS:
      return {
        ...state,
        pending: false
      };
    case ActionTypes.SET_CURRENT_AUTHOR:
      return {
        ...state,
        current: Object.assign({}, action.author)
      };

    case ActionTypes.RESET_CURRENT_AUTHOR:
      return {
        ...state,
        current: initialState.authors.current
      };
    default:
      return state;
  }
};

