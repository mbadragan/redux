/**
 * Created by badra on 8/4/2016.
 */

export default {
  courses: {
    list: [],
    current: {
      title: '',
      watchHref: '',
      authorId: '',
      length: '',
      category: ''
    },
    pending: false
  },

  authors: {
    list: [],
    current: {
      firstName: '',
      lastName: ''
    },
    pending: false
  }
}
