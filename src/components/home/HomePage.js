/**
 * Created by badra on 8/1/2016.
 */
import React from 'react';
import {Link} from 'react-router';

class HomePage extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <h1>Pluralsight Administration</h1>
        <p>some fancy paragraph here</p>
        <Link to="about" className="btn btn-default">Learn More</Link>
      </div>
    );
  }
}

export default HomePage;
