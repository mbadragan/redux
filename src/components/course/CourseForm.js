import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';

class CourseForm extends React.Component {
  constructor(props, context) {
    super(props, context);

  }

  render() {
    let props = this.props;
    // console.log('props at course form', props);
    return (
      <form onSubmit={props.onSubmit}>
        <TextInput
          label="Title"
          name="title"
          value={props.course.title}
          onChange={props.onChange}
          placeholder="Title"/>
        <TextInput
          label="url"
          name="watchHref"
          value={props.course.watchHref}
          onChange={props.onChange}
          placeholder="url"/>

        <SelectInput
          label="author"
          name="authorId"
          value={props.course.authorId}
          onChange={props.onChange}
          options={props.authors}
          placeholder="author"/>

        <TextInput
          label="category"
          name="category"
          value={props.course.category}
          onChange={props.onChange}
          placeholder="category"/>
        <br />
        <button className="btn btn-success" type="submit">Save</button>
      </form>
    );
  }
}

CourseForm.propTypes = {};

export default CourseForm;
