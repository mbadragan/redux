/**
 * Created by badra on 8/1/2016.
 */
import React from 'react';
import {connect} from 'react-redux';
import * as CourseActions from '../../actions/courseActions';
import {bindActionCreators} from 'redux';
import CourseList from './CourseList';
import {Link} from 'react-router';

class CoursePage extends React.Component {

  constructor (props, context) {
    super(props, context);
  }

  componentWillMount() {
    this.props.actions.resetCurrentCourse();
    this.props.actions.getAllCourses();
  }

  render() {
    let props = this.props;
    // console.log(props.courses);

    return (
      <div>
        <h1>Course</h1>
        <p>this is the course page</p>
        <Link to="courses/add" className="btn btn-primary">Add Course</Link>
        <CourseList courses={props.courses} />
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    courses: state.courses.list
  };
}

CoursePage.propTypes = {
  courses: React.PropTypes.array.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    // createCourse: (course) => dispatch(coursesActions.createCourse(course));
    actions: bindActionCreators(CourseActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CoursePage);

