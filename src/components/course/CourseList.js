/**
 * Created by badra on 8/2/2016.
 */

import React from 'react';
import {Link} from 'react-router';

class CourseList extends React.Component {

  render() {
    return (
      <div>
        <table className="table">
          <thead>
          <tr>
            <th>id</th>
            <th>title</th>
            <th>watchHref</th>
            <th>authorId</th>
            <th>length</th>
            <th>category</th>
          </tr>
          </thead>
          <tbody>
          {this.props.courses.map((course, index) => {
            return (
              <tr key={index}>
                <td><Link to={"/courses/" + course.id}>{course.id}</Link></td>
                <td>{course.title}</td>
                <td>{course.watchHref}</td>
                <td>{course.authorId}</td>
                <td>{course.length}</td>
                <td>{course.category}</td>
              </tr>
            );
          })}
          </tbody>
        </table>
      </div>
    );
  }
}

CourseList.propTypes = {
  courses: React.PropTypes.array.isRequired
  // actions: React.PropTypes.object.isRequired
};

export default CourseList;
