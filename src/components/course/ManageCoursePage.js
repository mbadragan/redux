import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';
import * as authorActions from '../../actions/authorActions';
import CourseForm from './CourseForm';

class ManageCoursePage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    this.props.actions.getAllAuthors();
    if (this.props.params.id) {
      this.props.actions.getCourseById(this.props.params.id);
    }
  }

  onSubmit(e) {
    e.preventDefault(e);
    this.props.actions.saveCourse(this.props.course);
  }

  onChange(e) {
    let props = this.props;
    let course = {...props.course};
    course[e.target.name] = e.target.value;

    props.actions.setCurrentCourse(course);
  }

  render() {
    let props = this.props;

    return (
      <div>
        <h1>Manage Course Page</h1>
        <CourseForm
          course={props.course}
          authors={props.authors}
          onSubmit={this.onSubmit}
          onChange={this.onChange}/>
      </div>
    );
  }
}

ManageCoursePage.propTypes = {
  course: React.PropTypes.object.isRequired
};

function mapStateToProps(state, props) {

  let authorOptions = state.authors.list.map((author) => {
    return {
      value: author.id,
      label: author.firstName + ' ' + author.lastName
    }
  });

  return {
    authors: authorOptions,
    course: state.courses.current
  };
}

function mapDispatchToProps(dispatch, props) {
  return {
    actions: bindActionCreators(Object.assign({}, courseActions, authorActions), dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageCoursePage);
