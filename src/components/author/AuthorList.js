/**
 * Created by badra on 8/2/2016.
 */

import React from 'react';
import {Link} from 'react-router';

class AuthorList extends React.Component {

  render() {
    return (
      <div>
        <table className="table">
          <thead>
          <tr>
            <th>id</th>
            <th>first name</th>
            <th>last name</th>
          </tr>
          </thead>
          <tbody>
          {this.props.authors.map((author, index) => {
            return (
              <tr key={index}>
                <td><Link to={"/authors/" + author.id}>{author.id}</Link></td>
                <td>{author.firstName}</td>
                <td>{author.lastName}</td>
              </tr>
            );
          })}
          </tbody>
        </table>
      </div>
    );
  }
}

AuthorList.propTypes = {
  authors: React.PropTypes.array.isRequired
  // actions: React.PropTypes.object.isRequired
};

export default AuthorList;
