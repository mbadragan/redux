import React from 'react';
import TextInput from '../common/TextInput';

class AuthorForm extends React.Component {
  constructor(props, context) {
    super(props, context);

  }

  render() {
    let props = this.props;
    // console.log('props at author form', props);
    return (
      <form onSubmit={props.onSubmit}>
        <TextInput
          label="First Name"
          name="firstName"
          value={props.author.firstName}
          onChange={props.onChange}
          placeholder="first name"/>
        <TextInput
          label="Last Name"
          name="lastName"
          value={props.author.lastName}
          onChange={props.onChange}
          placeholder="last name"/>

        <br />
        <button className="btn btn-success" type="submit">Save</button>
      </form>
    );
  }
}

AuthorForm.propTypes = {};

export default AuthorForm;
