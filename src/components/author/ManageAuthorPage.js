import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as authorActions from '../../actions/authorActions';
import AuthorForm from './AuthorForm';

class ManageAuthorPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    if (this.props.params.id) {
      this.props.actions.getAuthorById(this.props.params.id);
    }
  }

  onSubmit(e) {
    e.preventDefault(e);
    this.props.actions.saveAuthor(this.props.author);
  }

  onChange(e) {
    let props = this.props;
    let author = {...props.author};
    author[e.target.name] = e.target.value;

    props.actions.setCurrentAuthor(author);
  }

  render() {
    let props = this.props;

    return (
      <div>
        <h1>Manage Author Page</h1>
        <AuthorForm
          author={props.author}
          onSubmit={this.onSubmit}
          onChange={this.onChange}/>
      </div>
    );

  }
}

ManageAuthorPage.propTypes = {
  author: React.PropTypes.object.isRequired
};

function mapStateToProps(state, props) {
  return {
    author: state.authors.current
  };
}

function mapDispatchToProps(dispatch, props) {
  return {
    actions: bindActionCreators(authorActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageAuthorPage);
