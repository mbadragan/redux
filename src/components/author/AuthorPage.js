/**
 * Created by badra on 8/1/2016.
 */
import React from 'react';
import {connect} from 'react-redux';
import * as AuthorActions from '../../actions/authorActions';
import {bindActionCreators} from 'redux';
import AuthorList from './AuthorList';
import {Link} from 'react-router';

class AuthorPage extends React.Component {

  constructor (props, context) {
    super(props, context);
  }

  componentWillMount() {
    this.props.actions.getAllAuthors();
  }

  render() {
    let props = this.props;
    console.log(props.authors);

    return (
      <div>
        <h1>Author</h1>
        <p>this is the author page</p>
        <Link to="authors/add" className="btn btn-primary">Add Author</Link>
        <AuthorList authors={props.authors} />
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    authors: state.authors.list
  };
}

AuthorPage.propTypes = {
  authors: React.PropTypes.array.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    // createAuthor: (author) => dispatch(authorsActions.createAuthor(author));
    actions: bindActionCreators(AuthorActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorPage);

