import React from 'react';

class TextInput extends React.Component {

  render() {
    let props = this.props;

    return (
      <div>
        <label>{props.label}</label>
        <input
          type="text"
          className="form-control"
          name={props.name}
          placeholder={props.placeholder}
          value={props.value}
          onChange={props.onChange}/>
        <div>{props.error}</div>
      </div>
    );
  }
}

//define proptypes
TextInput.propTypes = {};

export default TextInput;
