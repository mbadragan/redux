/**
 * Created by badra on 8/1/2016.
 */

import React, {ProtpTypes} from 'react';
import {Link, IndexLink} from 'react-router';

const Header = () => {
  return (
    <nav>
      <IndexLink to="/" activeClassName="active">Home</IndexLink>
      {' | '}
      <Link to="/about" activeClassName="active">About</Link>
      {' | '}
      <Link to="/courses" activeClassName="active">Course</Link>
      {' | '}
      <Link to="/authors" activeClassName="active">Authors</Link>
    </nav>
  );
};

export default Header;
