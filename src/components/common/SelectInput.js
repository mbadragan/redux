import React from 'react';

class SelectInput extends React.Component {
  constructor(props, context) {
    super(props, context);

  }

  render() {
    let props = this.props;

    return (
      <div>
        <label>{props.label}</label>

        <select
          className="form-control"
          name={props.name}
          value={props.value}
          onChange={props.onChange} >
          <option value="">Please select an action</option>
          {props.options.map((option, index) => {
            return (
              <option key={index} value={option.value}>{option.label}</option>
            );
          })}
        </select>

        <div>{props.error}</div>
      </div>
    );
  }
}

SelectInput.propTypes = {};

export default SelectInput;
